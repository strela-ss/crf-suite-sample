# Data generation #

usage: data_generator.py [-h] [--train TRAIN] [--test TEST]

# Commands for training #

* _--pretrain_ - generates file with 3 columns using only first and last column from input file
* _--pretagged_ - if set, than input file should have at least 3 columns: word, tag, meaning
* _--train_ [FILE] - file to train on
* _--test_ [FILE [FILE [...]]] - list if files to test model
* _--algo_ - algorithm to train model with, choices are: 'lbfgs', 'l2sgd', 'ap', 'pa', 'arow'. Notice, when you train
 model with specified algorithm it would be saved to separate file so if you want to use this model in tests you should specify this parameter.

# Benchmarks #

lbfgs: 108.90 sec

             precision    recall  f1-score   support

      I-LOC       0.89      0.83      0.86      2094
     B-MISC       0.00      0.00      0.00         4
     I-MISC       0.89      0.80      0.84      1264
      I-ORG       0.83      0.79      0.81      2090
      I-PER       0.91      0.92      0.91      3149

    avg/total     0.88      0.85      0.86      8601

-----------------------------------------------------

l2sgd: 38.44 sec

             precision    recall  f1-score   support

      I-LOC       0.88      0.83      0.85      2094
     B-MISC       0.00      0.00      0.00         4
     I-MISC       0.91      0.78      0.84      1264
      I-ORG       0.80      0.79      0.80      2090
      I-PER       0.92      0.90      0.91      3149

    avg/total     0.88      0.84      0.86      8601

-----------------------------------------------------

ap: 16.72 sec

             precision    recall  f1-score   support

      I-LOC       0.92      0.87      0.90      2094
     B-MISC       0.40      0.50      0.44         4
     I-MISC       0.90      0.81      0.85      1264
      I-ORG       0.88      0.82      0.85      2090
      I-PER       0.93      0.94      0.93      3149

    avg/total     0.91      0.87      0.89      8601

-----------------------------------------------------

*pa: 17.56 sec*

             precision    recall  f1-score   support

      I-LOC       0.92      0.88      0.90      2094
     B-MISC       0.67      0.50      0.57         4
     I-MISC       0.89      0.82      0.85      1264
      I-ORG       0.89      0.82      0.85      2090
      I-PER       0.93      0.93      0.93      3149

    avg/total     0.91      0.88      0.89      8601

-----------------------------------------------------

arow: 15.49 sec

             precision    recall  f1-score   support

      I-LOC       0.87      0.88      0.87      2094
     B-MISC       0.33      0.25      0.29         4
     I-MISC       0.86      0.78      0.82      1264
      I-ORG       0.85      0.79      0.82      2090
      I-PER       0.93      0.92      0.92      3149

    avg/total     0.88      0.86      0.87      8601

-----------------------------------------------------
