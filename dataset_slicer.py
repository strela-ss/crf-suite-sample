import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parameters for slicer.')
    parser.add_argument('--input', '-i', help='Name of input file.')
    parser.add_argument('--output', '-o', help='Names of output file.')
    args = parser.parse_args()

    with open(args.input, 'r') as inp:
        with open(args.output or 'data/train.small', 'w') as out:
            for _ in range(1000000):
                out.write(inp.readline())
