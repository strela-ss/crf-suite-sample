import argparse
import os
from itertools import chain

import nltk
import pycrfsuite
import time
from sklearn.metrics import classification_report
from sklearn.preprocessing import LabelBinarizer


def timer(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        print('%r %2.2f sec' % (method.__name__, te - ts))
        return result

    return timed


@timer
def load_sentences_from_file(filename, lower):
    sentences = []
    with open(filename, 'r') as f:
        sentence = []
        for line in f:
            line = line.strip()
            if lower:
                line = line.lower()
            if line:
                sentence += [tuple(line.split(' '))]
            else:
                sentences += [sentence]
                sentence = []
    return sentences


def word2features(sent, i, pretagged):
    word = sent[i][0]
    postag = sent[i][1] if pretagged else nltk.pos_tag(nltk.word_tokenize(sent[i][0]))[0][1]
    features = [
        'bias',
        'word.lower=' + word.lower(),
        'word[-3:]=' + word[-3:],
        'word[-2:]=' + word[-2:],
        'word.isupper=%s' % word.isupper(),
        'word.istitle=%s' % word.istitle(),
        'word.isdigit=%s' % word.isdigit(),
        'postag=' + postag,
        'postag[:2]=' + postag[:2],
    ]
    if i > 0:
        word1 = sent[i - 1][0]
        postag1 = sent[i - 1][1]
        features.extend([
            '-1:word.lower=' + word1.lower(),
            '-1:word.istitle=%s' % word1.istitle(),
            '-1:word.isupper=%s' % word1.isupper(),
            '-1:postag=' + postag1,
            '-1:postag[:2]=' + postag1[:2],
        ])
    else:
        features.append('BOS')

    if i < len(sent) - 1:
        word1 = sent[i + 1][0]
        postag1 = sent[i + 1][1]
        features.extend([
            '+1:word.lower=' + word1.lower(),
            '+1:word.istitle=%s' % word1.istitle(),
            '+1:word.isupper=%s' % word1.isupper(),
            '+1:postag=' + postag1,
            '+1:postag[:2]=' + postag1[:2],
        ])
    else:
        features.append('EOS')

    return features


def sent2features(sent, pretagged):
    return [word2features(sent, i, pretagged) for i in range(len(sent))]


def sent2labels(sent):
    return [i[-1] for i in sent]


def sent2tokens(sent):
    return [i[0] for i in sent]


@timer
def crf_train(x_train, y_train, algorithm, model='default'):
    trainer = pycrfsuite.Trainer(verbose=False)
    for xseq, yseq in zip(x_train, y_train):
        trainer.append(xseq, yseq)

    trainer.set_params({
        'c1': 1.0,  # coefficient for L1 penalty
        'c2': 1e-3,  # coefficient for L2 penalty
        'max_iterations': 5000,  # stop earlier

        # include transitions that are possible, but not observed
        'feature.possible_transitions': True
    })

    if algorithm:
        trainer.select(algorithm)
        model = algorithm

    if not os.path.exists('./models'):
        os.makedirs('./models')

    model = 'models/%s.crfsuite' % model
    trainer.train(model)


def prediction(model):
    if not model:
        model = 'default'
    model = 'models/%s.crfsuite' % model
    tagger = pycrfsuite.Tagger()
    tagger.open(model)
    return tagger


@timer
def bio_classification_report(y_true, y_pred):
    """
    Classification report for a list of BIO-encoded sequences.
    It computes token-level metrics and discards "O" labels.

    Note that it requires scikit-learn 0.15+ (or a version from github master)
    to calculate averages properly!
    """
    lb = LabelBinarizer()
    y_true_combined = lb.fit_transform(list(chain.from_iterable(y_true)))
    y_pred_combined = lb.transform(list(chain.from_iterable(y_pred)))

    tagset = set(lb.classes_) - {'O'}
    tagset = sorted(tagset, key=lambda tag: tag.split('-', 1)[::-1])
    class_indices = {cls: idx for idx, cls in enumerate(lb.classes_)}

    return classification_report(
        y_true_combined,
        y_pred_combined,
        labels=[class_indices[cls] for cls in tagset],
        target_names=tagset,
    )


def parse_sentence(sentence):
    prepared_data = nltk.pos_tag(nltk.word_tokenize(sentence))
    tags = tagger.tag(sent2features(prepared_data, True))
    result = []
    for i, tag in enumerate(tags):
        if tag != 'O':
            result += [{
                'word': prepared_data[i][0],
                'tag': tag
            }]
    return result


def pretrain(filename):
    with open(filename, 'r') as inp:
        with open(filename + '.pretrained', 'w') as out:
            for line in inp:
                parts = line.split(' ')
                if len(parts) > 1:
                    new_line = ' '.join(nltk.pos_tag(nltk.word_tokenize(parts[0]))[0]) + ' ' + parts[-1]
                    out.write(new_line)
                else:
                    out.write('\n')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parameters for ner.')
    parser.add_argument('--train', help='Name of file to train on.')
    parser.add_argument('--test', nargs='*', help='Names of files to test on.')
    parser.add_argument('--pretagged', action='store_true', help='If you have already generated tags.')
    parser.add_argument('--pretrain', help='If you have already generated tags.')
    parser.add_argument('-l', '--lower', help='Casts all input text to lowercase.')
    parser.add_argument(
        '--algo',
        choices=['lbfgs', 'l2sgd', 'ap', 'pa', 'arow'],
        default='pa',
        help="""
            The name of the training algorithm.

            * 'lbfgs' for Gradient descent using the L-BFGS method,
            * 'l2sgd' for Stochastic Gradient Descent with L2 regularization term
            * 'ap' for Averaged Perceptron
            * 'pa' for Passive Aggressive
            * 'arow' for Adaptive Regularization Of Weight Vector
            """
    )
    args = parser.parse_args()

    if args.pretrain:
        pretrain(args.pretrain)

    if args.train:
        print("Loading train data from file <%s>..." % args.train)
        train = load_sentences_from_file(args.train, args.lower)

        x_train = [sent2features(s, args.pretagged) for s in train]
        y_train = [sent2labels(s) for s in train]

        print("Training...")
        crf_train(x_train, y_train, args.algo)

    tagger = prediction(args.algo)

    if args.test:
        for test_file in args.test:
            print("Loading test data from file <%s> ..." % test_file)
            test = load_sentences_from_file(test_file, args.lower)

            x_test = [sent2features(s, args.pretagged) for s in test]
            y_test = [sent2labels(s) for s in test]

            y_pred = [tagger.tag(xseq) for xseq in x_test]

            print('Generating report...')
            report = bio_classification_report(y_test, y_pred)
            print(report)

    SENTENCE = """
    hey there! my name is vlad and i'd like to open an account for "cofid corparate services limited"
    """

    nltk.pprint(parse_sentence(SENTENCE))
