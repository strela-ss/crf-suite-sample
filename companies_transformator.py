import argparse
import csv

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parameters for ner.')
    parser.add_argument('--rows', default='10000', help='Number of rows to output.')
    parser.add_argument('--out', default='data/companies_short.csv', help='Name of output file.')
    args = parser.parse_args()

    count = 0

    with open('companies_full.csv', 'r') as companies_in_file:
        with open('data/companies_short.csv', 'w') as companies_out_file:
            companies_in = csv.DictReader(companies_in_file)
            fieldnames = ['name', 'number']
            companies_out = csv.DictWriter(companies_out_file, fieldnames=fieldnames)
            companies_out.writeheader()
            for i in companies_in:
                if count < int(args.rows):
                    companies_out.writerow({'name': i['CompanyName'], 'number': i[' CompanyNumber']})
                    count += 1
                else:
                    break
