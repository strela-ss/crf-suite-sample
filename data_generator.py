import argparse
import json
import csv
import random
from pprint import pprint

import nltk

TEMPLATES_FORMAT = [
    """Hey there, Anna! I'm {name} and I'd like to open a business account for my '{company}'""",
    """Anna, my name is {name}. I need a new account for my business '"{company}'""",
    """Anna. My name is {name} and I am in need of a business account for "{company}" """,
    """Hey Anna, I'm {name}! My company "{company}" is in need of a business account.""",
    """Anna, I'm {name}. I am interested in opening an account for my company "{company}" """,
    """Hi Anna - My name is {name}. Can I open an account?""",
    """Anna, are you there? My name is {name} and I'd like to open an account. Is this possible?""",
    """Anna, I'm {name}! Is it possible to open an account right now?""",
    """is that Anna? My name is {name} and I think I'd like to open an account. How can I do that?""",
    """is that Anna? My name is {name} and I think I'd like to open an account. Can you help me please?""",
    """I want to open account for my company "{company}" """,
]

TEMPLATES = [
    """Hey there, Anna! I'm and I'd like to open a business account for my ' '""",
    """Anna, my name is. I need a new account for my business '' """,
    """Anna. My name is and I am in need of a business account for "" """,
    """Hey Anna, I'm ! My company "" is in need of a business account.""",
    """Anna, I'm . I am interested in opening an account for my company "" """,
    """Hi Anna - My name is . Can I open an account?""",
    """Anna, are you there? My name is and I'd like to open an account. Is this possible?""",
    """Anna, I'm ! Is it possible to open an account right now?""",
    """is that Anna? My name is and I think I'd like to open an account. How can I do that?""",
    """is that Anna? My name is and I think I'd like to open an account. Can you help me please?""",
    """I want to open account for my company "" """,
]

TOKENIZED = [
    {'data': [['Hey', 'NNP', 'O'],
              ['there', 'RB', 'O'],
              [',', ',', 'O'],
              ['Anna', 'NNP', 'O'],
              ['!', '.', 'O'],
              ['I', 'PRP', 'O'],
              ["'m", 'VBP', 'O'],
              ['and', 'CC', 'O'],
              ['I', 'PRP', 'O'],
              ["'d", 'MD', 'O'],
              ['like', 'VB', 'O'],
              ['to', 'TO', 'O'],
              ['open', 'VB', 'O'],
              ['a', 'DT', 'O'],
              ['business', 'NN', 'O'],
              ['account', 'NN', 'O'],
              ['for', 'IN', 'O'],
              ['my', 'PRP$', 'O'],
              ['``', '``', 'O'],
              ['``', '``', 'O']],
     'name': 6,
     'company': 18},
    {'data': [['Anna', 'NNP', 'O'],
              [',', ',', 'O'],
              ['my', 'PRP$', 'O'],
              ['name', 'NN', 'O'],
              ['is', 'VBZ', 'O'],
              ['.', '.', 'O'],
              ['I', 'PRP', 'O'],
              ['need', 'VBP', 'O'],
              ['a', 'DT', 'O'],
              ['new', 'JJ', 'O'],
              ['account', 'NN', 'O'],
              ['for', 'IN', 'O'],
              ['my', 'PRP$', 'O'],
              ['business', 'NN', 'O'],
              ['``', '``', 'O'],
              ['``', '``', 'O']],
     'name': 4,
     'company': 14},
    {'data': [['Anna', 'NNP', 'O'],
              ['.', '.', 'O'],
              ['My', 'PRP$', 'O'],
              ['name', 'NN', 'O'],
              ['is', 'VBZ', 'O'],
              ['and', 'CC', 'O'],
              ['I', 'PRP', 'O'],
              ['am', 'VBP', 'O'],
              ['in', 'IN', 'O'],
              ['need', 'NN', 'O'],
              ['of', 'IN', 'O'],
              ['a', 'DT', 'O'],
              ['business', 'NN', 'O'],
              ['account', 'NN', 'O'],
              ['for', 'IN', 'O'],
              ['``', '``', 'O'],
              ['``', '``', 'O']],
     'name': 4,
     'company': 15},
    {'data': [['Hey', 'NNP', 'O'],
              ['Anna', 'NNP', 'O'],
              [',', ',', 'O'],
              ['I', 'PRP', 'O'],
              ["'m", 'VBP', 'O'],
              ['!', '.', 'O'],
              ['My', 'PRP$', 'O'],
              ['company', 'NN', 'O'],
              ['``', '``', 'O'],
              ['``', '``', 'O'],
              ['is', 'VBZ', 'O'],
              ['in', 'IN', 'O'],
              ['need', 'NN', 'O'],
              ['of', 'IN', 'O'],
              ['a', 'DT', 'O'],
              ['business', 'NN', 'O'],
              ['account', 'NN', 'O'],
              ['.', '.', 'O']],
     'name': 4,
     'company': 8},
    {'data': [['Anna', 'NNP', 'O'],
              [',', ',', 'O'],
              ['I', 'PRP', 'O'],
              ["'m", 'VBP', 'O'],
              ['.', '.', 'O'],
              ['I', 'PRP', 'O'],
              ['am', 'VBP', 'O'],
              ['interested', 'JJ', 'O'],
              ['in', 'IN', 'O'],
              ['opening', 'VBG', 'O'],
              ['an', 'DT', 'O'],
              ['account', 'NN', 'O'],
              ['for', 'IN', 'O'],
              ['my', 'PRP$', 'O'],
              ['company', 'NN', 'O'],
              ['``', '``', 'O'],
              ['``', '``', 'O']],
     'name': 3,
     'company': 15},
    {'data': [['Hi', 'NNP', 'O'],
              ['Anna', 'NNP', 'O'],
              ['-', ':', 'O'],
              ['My', 'PRP$', 'O'],
              ['name', 'NN', 'O'],
              ['is', 'VBZ', 'O'],
              ['.', '.', 'O'],
              ['Can', 'MD', 'O'],
              ['I', 'PRP', 'O'],
              ['open', 'VB', 'O'],
              ['an', 'DT', 'O'],
              ['account', 'NN', 'O'],
              ['?', '.', 'O']],
     'name': 5,
     'company': -1},
    {'data': [['Anna', 'NNP', 'O'],
              [',', ',', 'O'],
              ['are', 'VBP', 'O'],
              ['you', 'PRP', 'O'],
              ['there', 'RB', 'O'],
              ['?', '.', 'O'],
              ['My', 'PRP$', 'O'],
              ['name', 'NN', 'O'],
              ['is', 'VBZ', 'O'],
              ['and', 'CC', 'O'],
              ['I', 'PRP', 'O'],
              ["'d", 'MD', 'O'],
              ['like', 'VB', 'O'],
              ['to', 'TO', 'O'],
              ['open', 'VB', 'O'],
              ['an', 'DT', 'O'],
              ['account', 'NN', 'O'],
              ['.', '.', 'O'],
              ['Is', 'VBZ', 'O'],
              ['this', 'DT', 'O'],
              ['possible', 'JJ', 'O'],
              ['?', '.', 'O']],
     'name': 8,
     'company': -1},
    {'data': [['Anna', 'NNP', 'O'],
              [',', ',', 'O'],
              ['I', 'PRP', 'O'],
              ["'m", 'VBP', 'O'],
              ['!', '.', 'O'],
              ['Is', 'VBZ', 'O'],
              ['it', 'PRP', 'O'],
              ['possible', 'JJ', 'O'],
              ['to', 'TO', 'O'],
              ['open', 'VB', 'O'],
              ['an', 'DT', 'O'],
              ['account', 'NN', 'O'],
              ['right', 'RB', 'O'],
              ['now', 'RB', 'O'],
              ['?', '.', 'O']],
     'name': 3,
     'company': -1},
    {'data': [['is', 'VBZ', 'O'],
              ['that', 'IN', 'O'],
              ['Anna', 'NNP', 'O'],
              ['?', '.', 'O'],
              ['My', 'PRP$', 'O'],
              ['name', 'NN', 'O'],
              ['is', 'VBZ', 'O'],
              ['and', 'CC', 'O'],
              ['I', 'PRP', 'O'],
              ['think', 'VBP', 'O'],
              ['I', 'PRP', 'O'],
              ["'d", 'MD', 'O'],
              ['like', 'VB', 'O'],
              ['to', 'TO', 'O'],
              ['open', 'VB', 'O'],
              ['an', 'DT', 'O'],
              ['account', 'NN', 'O'],
              ['.', '.', 'O'],
              ['How', 'WRB', 'O'],
              ['can', 'MD', 'O'],
              ['I', 'PRP', 'O'],
              ['do', 'VB', 'O'],
              ['that', 'DT', 'O'],
              ['?', '.', 'O']],
     'name': 6,
     'company': -1},
    {'data': [['is', 'VBZ', 'O'],
              ['that', 'IN', 'O'],
              ['Anna', 'NNP', 'O'],
              ['?', '.', 'O'],
              ['My', 'PRP$', 'O'],
              ['name', 'NN', 'O'],
              ['is', 'VBZ', 'O'],
              ['and', 'CC', 'O'],
              ['I', 'PRP', 'O'],
              ['think', 'VBP', 'O'],
              ['I', 'PRP', 'O'],
              ["'d", 'MD', 'O'],
              ['like', 'VB', 'O'],
              ['to', 'TO', 'O'],
              ['open', 'VB', 'O'],
              ['an', 'DT', 'O'],
              ['account', 'NN', 'O'],
              ['.', '.', 'O'],
              ['Can', 'MD', 'O'],
              ['you', 'PRP', 'O'],
              ['help', 'VB', 'O'],
              ['me', 'PRP', 'O'],
              ['please', 'VB', 'O'],
              ['?', '.', 'O']],
     'name': 6,
     'company': -1},
    {'data': [['I', 'PRP', 'O'],
              ['want', 'VBP', 'O'],
              ['to', 'TO', 'O'],
              ['open', 'VB', 'O'],
              ['account', 'NN', 'O'],
              ['for', 'IN', 'O'],
              ['my', 'PRP$', 'O'],
              ['company', 'NN', 'O'],
              ['``', '``', 'O'],
              ['``', '``', 'O']],
     'name': -1,
     'company': 8},
]


def tokenize():
    global TOKENIZED
    for i in TEMPLATES:
        tokens = nltk.pos_tag(nltk.word_tokenize(i))
        TOKENIZED += [list(map(lambda x: list(x) + ['O'], tokens))]


def get_tokenized_sentence():
    num = int(random.random() * len(TOKENIZED))
    return TOKENIZED[num]


def get_name(names):
    name = names[int(random.random() * len(names))]
    if random.random() > 0.5:
        name = name.lower()
    return [name, 'NNP', 'I-PER']


def precess_company(company):
    return list(map(lambda x: [x, 'NNP', 'I-ORG'], company['CompanyName'].split(' ')))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parameters for data generator.')
    parser.add_argument('--train', help='Percentage of source file that would be stored in train file.')
    parser.add_argument('--test', help='Percentage of source file that would be stored in test file.')
    args = parser.parse_args()

    train = 0.05
    test = 0.05
    if args.train:
        if args.train.isdigit() and 0 < int(args.train) < 101:
            train = int(args.train) / 100
    if args.test:
        if args.test.isdigit() and 0 < int(args.test) < 101:
            test = int(args.test) / 100

    with open('names.json', 'r') as js:
        names = json.loads(js.read())

    with open('companies.csv') as companies_csv:
        num_companies = row_count = sum(1 for row in csv.reader(companies_csv))

    with open('companies.csv') as companies_csv:
        companies = csv.DictReader(companies_csv)

        with open('train.test', 'w') as train_file:
            with open('test.test', 'w') as test_file:
                for idx, i in enumerate(companies):
                    data = get_tokenized_sentence()
                    sentence = data['data']
                    if data['name'] > 0 and data['company'] > 0:
                        sentence = sentence[:data['name']+1] + [get_name(names)] + \
                                   sentence[data['name']+1:data['company']+1] + precess_company(i) + \
                                   sentence[data['company']+1:]
                    elif data['name'] > 0:
                        sentence = sentence[:data['name']+1] + [get_name(names)] + sentence[data['name']+1:]
                    elif data['company'] > 0:
                        sentence = sentence[:data['company']+1] + precess_company(i) + sentence[data['company']+1:]
                    sentence = '\n'.join([' '.join(i) for i in sentence]) + '\n\n'

                    if idx / num_companies < train:
                        train_file.write(sentence)
                    elif idx / num_companies > 1 - test:
                        test_file.write(sentence)
