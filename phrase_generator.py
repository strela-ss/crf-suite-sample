import argparse
import csv
import json
import random

import nltk


def generate_template(sentence, tags):
    tokens = nltk.pos_tag(nltk.word_tokenize(sentence))
    d = {
        'data': [],
        'placeholders': {}
    }
    for i, t in enumerate(tokens):
        if t[0] in tags.keys():
            d['placeholders'][t[0]] = i
        d['data'] += [[t[0], t[1], 'O']]
    return d


def generate_insertion(sentence, tag):
    tokens = nltk.pos_tag(nltk.word_tokenize(sentence))
    return list(map(lambda x: list(x) + [tag], tokens))


def generate_templates(templates_text, tags):
    return [generate_template(i, tags) for i in templates_text]


def generate_dataset(templates_text, filename, num_sentences, tags):
    templates = generate_templates(templates_text, tags)
    with open(filename, 'w') as out:
        for i in range(num_sentences):
            template = random.choice(templates)
            data = template['data']
            for key, value in sorted(template['placeholders'].items(), key=lambda x: -x[1]):
                r = random.choice(tags[key])
                insertion = generate_insertion(r, key)
                data = data[:value] + insertion + data[value + 1:]
            str_data = '\n'.join([' '.join(i) for i in data]) + '\n\n'
            out.write(str_data)


def load_persons(filename):
    try:
        with open(filename, 'r') as f:
            result = json.loads(f.read())
    except:
        result = ['Andrew', 'Anton', 'Den', 'Tom', 'Pavel']
    return result


def load_organizations(filename, fieldname):
    try:
        with open(filename, 'r') as f:
            reader = csv.DictReader(f)
            result = [i[fieldname] for i in reader]
    except:
        result = [
            """ BACHELORS LIMITED(THE) """,
            """ BAMEX ZAKLAD PRODUKCYJNO-HANDLOWY """,
            """ BATTLE'S OVER - A NATION'S TRIBUTE 11TH NOVEMBER 2018 LTD """,
            """ BE NEWHALL MANAGEMENT COMPANY LIMITED """,
            """ BEDE INVESTMENT PROPERTIES LIMITED """,
            """ BEECHBANK COURT MANAGEMENT COMPANY LIMITED """,
            """ BELLE-VUE ENTERPRISES LIMITED """,
            """ BELLINGHAM FOODS LTD """,
            """ BELMIN BANISTERS LTD """,
            """ BNY LIMITED """,
            """ BOBOTRANS LTD LTD """,
            """ BRANCHING - OUT AND IN LIMITED """,
            """ BRYANSTON SCHOOL, INCORPORATED """,
        ]
    return result


def load_templates(filename):
    try:
        with open(filename, 'r') as f:
            result = [i for i in f]
    except:
        result = [
            """Hey there, Anna! I'm I-PER and I'd like to open a business account for my "I-ORG" """,
            """Anna, my name is I-PER. I need a new account for my business "I-ORG" """,
            """Hey Anna, I'm I-PER! My company "I-ORG" is in need of a business account.""",
            """Anna, are you there? My name is I-PER and I'd like to open an account. Is this possible?""",
            """Anna, I'm I-PER! Is it possible to open an account right now?""",
            """is that Anna? My name is I-PER and I think I'd like to open an account. How can I do that?""",
            """I want to open account for my company "I-ORG" """,
        ]
    return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parameters for ner.')
    parser.add_argument('--templates', '-t', default='data/templates', help='File that contains templates.')
    parser.add_argument('--names', default='data/names.json', help='File that contains names, should be a json file.')
    parser.add_argument('--org', default='data/companies_short.csv',
                        help='File that contains names of organizations in csv format.')
    parser.add_argument('--org_field', default='name',
                        help='Name of field in csv file that conatins name of organization.')
    parser.add_argument('--sentences', '-s', default='10000',
                        help='Number of sentences that would be written in the output file.')
    parser.add_argument('--out', '-o', default='data/dataset', help='Name of output file.')
    args = parser.parse_args()

    templates_text = load_templates(args.templates)
    tags = {
        'I-PER': load_persons(args.names),
        'I-ORG': load_organizations(args.org, args.org_field)
    }
    generate_dataset(templates_text, args.out, int(args.sentences), tags)
